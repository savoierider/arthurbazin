<?php

//////////////////////////////////////////////////////////////////////////////////////////////////////
// Aide debug
//
// copyright Arthur Bazin
//////////////////////////////////////////////////////////////////////////////////////////////////////

// Informations
//////////////////////////////////////////////////////////////////////////////////////////////////////

// Préfixe : "debug_"

ajout test

// Définition des variables
//////////////////////////////////////////////////////////////////////////////////////////////////////

// Variables de configuration
// Niveau de log
//$GLOBALS['DEBUG']['niveau'] = ''; 					// 0 = Aucun, 1 = Production verbeux, 2 = 1+Débug fonction, 3 = 2+Debug variable
// Fichier de log
//$GLOBALS['DEBUG']['logfile'] = '';



// Définition des actions
//////////////////////////////////////////////////////////////////////////////////////////////////////

// Initialisation du fichier de log
debug_log(1, '---------------------------------------------------------------------');
debug_log(1, 'Initialisation du log');
debug_log(1, '---------------------------------------------------------------------');
debug_log(3, 'Variables générales : '."\r\n");
debug_log(3, '$_GET :'."\r\n".var_export($_GET, TRUE)."\r\n");
debug_log(3, '$_POST :'."\r\n".var_export($_POST, TRUE)."\r\n");
debug_log(3, 'Variables spécifiques : '."\r\n");
debug_log(3, '$GLOBAL[\'DEBUG\'] :'."\r\n".var_export($GLOBALS['DEBUG'], TRUE)."\r\n");



// Définition des fonctions
//////////////////////////////////////////////////////////////////////////////////////////////////////

// Fonction d'affichage des informations
////////////////////////////////////////

function debug_info_display() {
	echo '<pre>';
	echo 'session ID : '.session_id().'<br /><br />';
	echo '$_SESSION<br />';
	var_dump($_SESSION);
	echo '<br />$_POST<br />';
	var_dump($_POST);
	echo '<br />$_GET<br />';
	var_dump($_GET);
	//echo '<br />$_GLOBALS<br />';
	//var_dump($GLOBALS);
	echo '</pre>';
};



// Fonction d'affichage des informations
////////////////////////////////////////

function debug_get_info() {
	$message = 'Information sur les principales variables'."\r\n\r\n";
	$message .= 'Session ID : '.session_id()."\r\n\r\n";
	$message .= '$_SESSION :'."\r\n";
	$message .= var_export($_SESSION, TRUE)."\r\n\r\n";
	$message .= '$_GET :'."\r\n";
	$message .= var_export($_GET, TRUE)."\r\n\r\n";
	$message .= '$_POST :'."\r\n";
	$message .= var_export($_POST, TRUE)."\r\n\r\n";
	$message .= '$GLOBALS :'."\r\n";
	$message .= '$GLOBALS[\'GENERAL\'] :'."\r\n";
	$message .= var_export($GLOBALS['GENERAL'], TRUE)."\r\n";
	$message .= '$GLOBALS[\'MAIL\'] :'."\r\n";
	$message .= var_export($GLOBALS['MAIL'], TRUE)."\r\n";
	$message .= '$GLOBALS[\'AUTH\'] :'."\r\n";
	$message .= var_export($GLOBALS['AUTH'], TRUE)."\r\n";
	$message .= '$GLOBALS[\'DEBUG\'] :'."\r\n";
	$message .= var_export($GLOBALS['DEBUG'], TRUE)."\r\n";
	$message .= '$GLOBALS[\'ACCOUNT\'] :'."\r\n";
	$message .= var_export($GLOBALS['ACCOUNT'], TRUE)."\r\n";
	$message .= '$GLOBALS[\'ReCaptcha\'] :'."\r\n";
	$message .= var_export($GLOBALS['ReCaptcha'], TRUE)."\r\n";
	$message .= '$_COOKIE :'."\r\n";
	$message .= var_export($_COOKIE, TRUE)."\r\n";
	
	return $message;
};



// Fonction de log
//////////////////

function debug_log($niveau, $message, $gestion_fichier=FILE_APPEND) {
	if ( $niveau <= $GLOBALS['DEBUG']['niveau'] ) {
		// Création d'un date_time actuel
		$datetime_actuel =  date("Y-m-d H:i:s");

		// Création du message loggué
		$log = $datetime_actuel.' - '.$GLOBALS['GENERAL']['page_consult_ext'].' - '.$message."\r\n";

		if ( $gestion_fichier == 'init' ) {
			$gestion_fichier = 0;
		};

		// Ecriture
		file_put_contents($GLOBALS['DEBUG']['logfile'], $log, $gestion_fichier|LOCK_EX);
	}
};



?>