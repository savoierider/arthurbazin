<?php

//////////////////////////////////////////////////////////////////////////////////////////////////////
// Bibliothèque générale de fonctions PHP
//
// copyright Arthur Bazin
//////////////////////////////////////////////////////////////////////////////////////////////////////

// Informations
//////////////////////////////////////////////////////////////////////////////////////////////////////

// Préfixe : "general_"
// Ce fichier requière l'utilisation du fichier de configuration

test ajout
encore un autre ajout
// Définition des variables
//////////////////////////////////////////////////////////////////////////////////////////////////////

// Données POST nettoyées
$clean_POST = general_clean_array($_POST);

// Données GET nettoyées
$clean_GET = general_clean_array($_GET);

// Variables de configuration
require_once $_SERVER['DOCUMENT_ROOT'].'/'.'administration/configuration/variables.php';

// BDD
//$GLOBALS['FONC_GEN']['BDD'] = '';

// Statut des traitements de formulaires
$statut_traitement['log']['code'] = '0_0';
$statut_traitement['log']['message'] = 'Initialisation';
$statut_traitement['nom'] = 'Aucun';

if ( isset($clean_POST['post_action']) ) {
	$statut_traitement['nom'] = $clean_POST['post_action'];
}
elseif ( isset($_SESSION['statut_traitement']['nom']) ) {
	$statut_traitement = array_merge($statut_traitement, $_SESSION['statut_traitement']);

	unset($_SESSION['statut_traitement']);
}



// Définition des actions
//////////////////////////////////////////////////////////////////////////////////////////////////////

//--



// Définition des fonctions
//////////////////////////////////////////////////////////////////////////////////////////////////////




/* Fonction de connexion à une BDD
//////////////////////////////////
 
Fonction :
	general_connexion_bdd($1,$2)
Description : 
	Fonction permettant de se connecter à une BDD
Paramètres :
	$1 : Type de BDD (MYSQL/POSTGRESQL...)
	$2 : Nom de la BDD (Nom donné dans le fichier de stockage des paramètres)
Retours :
	Objet PDO de BDD
*/

// Fonction de connexion à la BDD d'authentification
////////////////////////////////////////////////////

function general_connexion_bdd($type, $bdd) {
	
	$conf_BDD = $GLOBALS['FONC_GEN']['BDD'][$type][$bdd];

	$bdd_dns = 
		$conf_BDD['driver'].
		':host=' . $conf_BDD['host'].
		';port=' . $conf_BDD['port'].
		';dbname=' . $conf_BDD['schema'].
		';charset=' . $conf_BDD['charset'];
	
	// Instantiation de la BDD
	try {
		$Connexion_BDD = new PDO($bdd_dns, $conf_BDD['username'], $conf_BDD['password'], $conf_BDD['options']);
	} catch (PDOException $e) {
		print "Erreur de connexion à la BDD ! Message : " . $e->getMessage() . "<br/>";
		die();
	}

	return $Connexion_BDD;
};



/* Fonction de nettoyage des variables tableau
//////////////////////////////////////////////
 
Fonction :
	general_clean_array($1)
Description : 
	Fonction permettant de nettoyer les valeurs d'une variable de code HTML malveillants
Paramètres :
	$1 : Variable (simple ou tableau)
Retours :
	Variable dont les valeurs ont été échappées.
*/

function general_clean_array($variable) {
	// Si la variable est un tableau
	if (is_array($variable)) {
		// Pour chaque valeur du tableau
		foreach ($variable as $key => $valeur) {
			// On relance la fonction
			$variable[$key]=general_clean_array($valeur);
		}
	}
	// S'il s'agit d'une variable simple
	else {
		// On retire les espaces surnuméraires et on échappe le HTML
		$variable = trim(htmlspecialchars($variable));
	}
	
	// On renvoi la variable
	return $variable;
}



/* Fonction de récupréation et de décodage JSON
///////////////////////////////////////////////
 
Fonction :
	general_recup_json($1)
Description : 
	Fonction permettant de récupérer les données d'un fichier JSON et de les mettre dans un tableau
Paramètres :
	$1 : Emplacement du fichier JSON
Retours :
	Tableau contenant les valeurs du fichier JSON
*/

function general_recup_json($fichier) {
	// Récupération du fichier
	$json_file = file_get_contents($fichier);
	
	// Convertion en UTF8 (au cas ou)
	$json_file = utf8_encode($json_file);
	
	// Décodage du JSON et mise en forme dans un tableau
	$json = json_decode($json_file, true);
	
	return $json;
};



/* Fonction de présence dans un tableau (équivalent à in_array() mais en mieux)
///////////////////////////////////////////////////////////////////////////////
 
Fonction :
	general_in_array($1,$2)
Description : 
	Fonction permettant de détecter si une valeur est présente dans un tableau
Paramètres :
	$1 : Tableau de valeur
		En faisant précéder les valeurs par un moins on renvoie l'inverse : mode négatif
	$2 : Valeur à chercher
Retours :
	TRUE : Valeur présente dans le tableau (Valeur absente du tableau en mode négatif)
	FALSE : Valeur absente du tableau (Valeur présente dans le tableau en mode négatif)
*/

function general_in_array($p_array_table = 'all', $p_table = '') {

	// Vérification de l'utilisation de toutes les tables
	if ( $p_array_table == 'all' ) {
		// On retourne vrai si 'all'
		return TRUE;
	};


	// Mode négatif ou mode positif
	if ( substr($p_array_table[0], 0, 1) == '-' ) {
		$mode = 'negatif';
	}
	else {
		$mode = 'positif';
	};


	// Vérification
	if ( $mode == 'positif' ) {
		foreach ($p_array_table as $f_table) {

			// Récupération du nom de la table
			if ( substr($f_table, 0, 1) == '+' ) {
				$f_table = substr($f_table, 1, strlen($f_table));
			};

			if ( $f_table == $p_table ) {
				return TRUE;
			};
		};
		return FALSE;
	}
	else {
		foreach ($p_array_table as $f_table) {

			// Récupération du nom de la table
			if ( substr($f_table, 0, 1) == '-' ) {
				$f_table = substr($f_table, 1, strlen($f_table));
			};

			if ( $f_table == $p_table ) {
				return FALSE;
			};
		};

		return TRUE;
	};
};



/* Fonction de suppression des accents
//////////////////////////////////////
 
Fonction :
	skip_accents($1,$2)
Description : 
	Fonction permettant de retrirer tous les caractères accentués et bizarre du français.
Paramètres :
	$1 : chaine à traiter
	$2 : encodage initial.
		Par défaut utf-8
Retours :
	Texte sans caractères spéciaux
*/

function skip_accents( $str, $charset='utf-8' ) {

	// Conversion des caractère en code HTML
	$str = htmlentities( $str, ENT_NOQUOTES, $charset );

	// Suppression des code HTML et conservation du caractère initial
	$str = preg_replace( '#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str );
	// Idem pour les ligatures
	$str = preg_replace( '#&([A-za-z]{2})(?:lig);#', '\1', $str );
	// Idem pour ...
	$str = preg_replace( '#&[^;]+;#', '', $str );
	
	return $str;
}



/* Fonction d'affichage d'un message d'information suite à la connexion
///////////////////////////////////////////////////////////////////////
 
Fonction :
	general_info_message_information_connexion($1,$2,$3)
Description : 
	Fonction permettant d'afficher un message d'information à la suite du traitement d'un formulaire
Paramètres :
	$1 : retour du traitement du formulaire
	$2 : Nom du traitement à analyser 
		null : tous retour de traitement est traité
		'nom_traitement' = Nom du traitement à analyser, contenu dans la variable $statut_traitement['nom']
Retours :
	Code HTML contenant le message à afficher
*/

function general_message_traitement_formulaire($retour_traitement, $nom_traitement='null') {

	// La fonction ne renvoie quelque chose que si on est dans le cas standard ou que l'action correspond
	if ( 
		(
			$nom_traitement <> 'null'
			AND isset($retour_traitement['nom'])
			AND $retour_traitement['nom'] <> $nom_traitement
		) 
		OR $retour_traitement['log']['code'] == '0_0'
		) {
		return '';
	}


	// Style du message
	// Vert
	$message['style']['1'] = 'succes';
	// Jaune
	$message['style']['2'] = 'attente';
	// Rouge
	$message['style']['3'] = 'erreur';

	// Assignation du style
	$code_style = substr($retour_traitement['log']['code'], 0, 1);
	if ( !isset($message['style'][$code_style]) ) {
		$code_style = 1;
	}


	// Icone d'accompagnement du message
	// OK dans un cerle
	$message['icone']['1'] = 'fas fa-check-circle';
	// Exclamation dans un cercle
	$message['icone']['2'] = 'fas fa-exclamation-circle';
	// Exclamation dans un triangle
	$message['icone']['3'] = 'fas fa-exclamation-triangle';

	// Assignation de l'icône
	$code_icone = substr($retour_traitement['log']['code'], 2, 1);
	if ( !isset($message['icone'][$code_icone]) ) {
		$code_icone = 1;
	}

	// Retrait des accents
	//$retour_traitement['log']['message'] = iconv( 'UTF-8', 'ASCII//TRANSLIT', $retour_traitement['log']['message'] );
	$retour_traitement['log']['message'] = skip_accents( $retour_traitement['log']['message'] );

	// Assignation du message
	$message['message'] = $retour_traitement['log']['message'];
	// Message technique
	//$message['message'] = 'Code : '.$retour_traitement['log']['code'].' - Message : '.$retour_traitement['log']['message'].' - Traitement : '.$retour_traitement['nom'];


	// Création du code HTML
	$retour = '<div><span class="form_message_info '.$message['style'][$code_style].'"><i class="'.$message['icone'][$code_icone].'"></i> '.$message['message'].'</span></div>';

	return $retour;
}



?>